import React, { useState } from 'react';

const RepoItem = ({ repo }) => {
  const [loaded, setLoaded] = useState(false);
  const [loading, setLoading] = useState(false);
  const [stargazers, setStargazers] = useState([]);

  const fetchStargazers = async () => {
    setLoading(true);
    const stargazers = await fetch(
      `https://api.github.com/repos/${repo.full_name}/stargazers`
    ).then(res => res.json());
    setLoading(false);
    setLoaded(true);
    setStargazers(stargazers);
  };

  return (
    <li>
      {repo.name}
      {!loaded && (
        <button onClick={() => fetchStargazers()}>
          {loading ? 'Loading...' : 'Load star gazers'}
        </button>
      )}
      {loaded && (
        <ul>
          {stargazers.map(gazer => (
            <li>{gazer.login}</li>
          ))}
        </ul>
      )}
    </li>
  );
};

const RepoList = ({ username, repos, loading }) => {
  if (loading) {
    return <div>Loading repos for {username}</div>;
  }
  return (
    <ul>
      {repos.map(repo => (
        <RepoItem repo={repo} />
      ))}
    </ul>
  );
};

const App = () => {
  const [username, setUsername] = useState('');
  const [loading, setLoading] = useState(false);
  const [repos, setRepos] = useState([]);

  const fetchRepo = async () => {
    setLoading(true);
    const repos = await fetch(
      `https://api.github.com/users/${username}/repos`
    ).then(res => res.json());
    setRepos(repos);
    setLoading(false);
  };

  const handleKeyUp = e => {
    if (e.keyCode === 13) {
      fetchRepo();
    }
  };

  return (
    <div>
      <input
        value={username}
        onChange={e => setUsername(e.target.value)}
        onKeyUp={e => handleKeyUp(e)}
      />
      <RepoList username={username} repos={repos} loading={loading} />
    </div>
  );
};

export default App;
